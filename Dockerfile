FROM registry.sindominio.net/debian as builder

RUN apt update -y && \
    apt install -y git nodejs npm

RUN git clone https://github.com/Johni0702/mumble-web /home/node

COPY ./config.local.js /home/node/app/config.local.js

RUN mkdir -p /home/node/.npm-global && \
    mkdir -p /home/node/app

ENV PATH=/home/node/.npm-global/bin:$PATH
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

RUN cd /home/node && \
    npm install && \
    npx browserslist@latest --update-db && \
    npm run build 

FROM registry.sindominio.net/debian

RUN apt update -y && \
    apt install -y tini websockify

COPY --from=builder /home/node/dist ./mumble
COPY ./config.local.js ./mumble/app/config.local.js

EXPOSE 8080
ENV MUMBLE_SERVER=mumble:64738

ENTRYPOINT ["/usr/bin/tini", "--"]
CMD websockify --ssl-target 8080 --web=/mumble "$MUMBLE_SERVER"
