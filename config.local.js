// You can overwrite the default configuration values set in [config.js] here.
// There should never be any required changes to this file and you can always
// simply copy it over when updating to a new version.

let config = window.mumbleWebConfig // eslint-disable-line no-unused-vars

// E.g. changing default address and theme:
//config.defaults.address = 'charlar.sindominio.net/mumble'
//config.defaults.theme = 'MetroMumbleDark' 

window.mumbleWebConfig = {
  // Which fields to show on the Connect to Server dialog
  'connectDialog': {
    'address': false,
    'port': false,
    'token': false,
    'username': true,
    'password': false,
    'channelName': false
  },
  // Default values for user settings
  // You can see your current value by typing `localStorage.getItem('mumble.$setting')` in the web console.
  'settings': {
    'voiceMode': 'ptt', // one of 'cont' (Continuous), 'ptt' (Push-to-Talk), 'vad' (Voice Activity Detection)
    'pttKey': 'ctrl + shift',
    'vadLevel': 0.3,
    'toolbarVertical': false,
    'showAvatars': 'always', // one of 'always', 'own_channel', 'linked_channel', 'minimal_only', 'never'
    'userCountInChannelName': false,
    'audioBitrate': 40000, // bits per second
    'samplesPerPacket': 960
  },
  // Default values (can be changed by passing a query parameter of the same name)
  'defaults': {
    // Connect Dialog
    'address': window.location.hostname,
    'port': '443/mumble/?webrtc=false', // try disable webrtc 
		'webrtc':false, // is not working... 
    'token': '',
    'username': '',
    'password': '',
    'joinDialog': false, // replace whole dialog with single "Join Conference" button
    'matrix': false, // enable Matrix Widget support (mostly auto-detected; implies 'joinDialog')
    'avatarurl': '', // download and set the user's Mumble avatar to the image at this URL
    // General
    'theme': 'MetroMumbleDark'
  }
}
